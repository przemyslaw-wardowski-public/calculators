package com.bff.djl.calculators

class BalanceCalculator implements Calculator {

	def calculate(Map ctx){
		if(ctx["data"])
			return ctx["data"] * 2

		throw new NoDataInCalculationContextException()
	}
}
