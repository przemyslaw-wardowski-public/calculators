package com.bff.djl.calculators

interface Calculator {
	def calculate(Map ctx)
}
