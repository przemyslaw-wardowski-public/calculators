package com.bff.djl.calculators

import spock.lang.Specification


class CalculatorsApplicationTests extends Specification{

	def "Calculation fails where data not present in context" () {
		when:
			new BalanceCalculator().calculate([:] as Map)
		then:
			thrown( NoDataInCalculationContextException )
	}

	def "Calculation return balance as doubled data" () {
		given:
			def calculator = new BalanceCalculator()
		and:
			def context = ['data' : 3] as Map
		expect:
			calculator.calculate(context) == 6
	}
}
